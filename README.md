# JDR

## Fiche de personnage :

1. Nom
2. Age
3. Sexe
4. Taille
5. Poids
6. Description
7. Race
8. Classe
9. Niveau
10. Caractèristiques
11. Equipements


## Races :

### Demi-Elfe

```
Le Demi-elfe a hérité des qualités de ses deux parents (humain et Elfe). Il est généralement traité avec condescendance parmi les Elfes et est considéré comme un Elfe parmi les humains.
Préjugés typiques : les Demi-elfes sont des artistes hors-pair dotés d’une grande sensibilité. Toutefois, attention car ils sont beaux parleurs et manipulateurs : cachez vos filles et vos femmes, ils ont brisé plus d’un cœur et corrompu bien des dames de réputation vertueuse.

```

> Repères

```
Âge de départ : 20+
Espérance de vie : 150 ans
Taille : 1.50 m à 1.90 m
Poids : 40 kg à 80 kg
Grâce naturelle, oreilles légèrement pointues, pilosité faible.

```

> Création de personnage

```
Carac : +2 SAG, -2 CON
Capacité raciale • Sang mêlé : le demi-elfe choisi l'une des deux capacités suivantes.
Instinct de survie (humain) : Lorsqu’une attaque devrait amener le personnage à 0 PV, les DM qu’elle inflige sont divisés par 2 (minimum 1).
Lumière des étoiles (elfe) : Pour un haut elfe, l’obscurité de la nuit sous la lumière des étoiles n’est que de la pénombre, où seuls les petits détails lui échappent.

```

> Noms typiques

```
Les noms des personnages demi-elfes reflètent souvent leur double nature et ils se composent d’un prénom elfique et d’un nom humain. Plus rarement l’inverse, car les elfes n’acceptent pas qu’une créature avec une durée de vie aussi brève porte le nom de leur lignée.
```

### Demi-Orque

```
Le Demi-Orque est de grande taille. Issu de l’union souvent forcée d’une humaine et d’un Orque, il possède une force physique hors du commun. Il est généralement méprisé par les autres races, en particulier par les Nains et les Elfes.

Préjugés typiques : Les Demi-Orques sont violents, sales et stupides, ils en veulent au monde entier de leur ignoble naissance et mieux vaut les éviter.

```

> Repères

```
Âge de départ : 15+
Espérance de vie : 60 ans
Taille : 1.70 m à 2.10 m
Poids : 70 kg à 150 kg
Grand et athlétique. Peau grisâtre ou verdâtre, mâchoire large, front bas, petits yeux.

```

> Création de personnage

```
Carac : +2 FOR, -2 INT, -2 CHA
Capacité raciale · Vision dans le noir : dans le noir total, le Demi-Orque voit comme dans la pénombre jusqu’à 30 m.

```

> Noms typiques

```
Les Demi-orques utilisent des prénoms humains, à moins qu’ils aient été élevés chez les Orques. Certains se choisissent aussi un prénom orque à l’âge adulte, en réaction à l’ostracisme dont ils sont victimes. Les noms orques sont courts et gutturaux, privilégiez les lettres R, G et K, évitez une voyelle douce comme le E et préférez le O.

Masculin. Aog, Bargul, Carok, Drog, Erok, Farg, Gorog, Hurl, Jorgul, Krok, Krush, Lug, Mog, Norok, Rok, Urdu, etc.

Féminin. Les femmes orques ajoutent généralement la lettre A à la fin de leur prénom.

```

### Elfe-Haut

```
Le Haut-Elfe est un être féerique qui vit extrêmement longtemps. Il est méprisant envers les Nains et arrogant envers les autres races. Proche de la nature, il maîtrise aussi bien les arts de la magie que ceux de la guerre.

Préjugés typiques : les Hauts-Elfes se croient supérieurs à tous les autres races, ce sont de puissants magiciens mais leur cœur est sans pitié. Ils obéissent à de très anciennes règles et à des serments incompréhensibles pour les races à l’espérance de vie ordinaire.

```

> Repères

```
Âge de départ : 80+
Espérance de vie : 450 ans
Taille : 1.50 m à 1.80 m
Poids : 40 kg à 70 kg
Svelte et gracieux. Oreilles pointues, yeux en amandes (verts, violets), cheveux parfois blancs, argent ou or, pilosité absente.

```

> Création de personnage

```
Carac : -2 FOR, +2 CHA
Capacité raciale · Lumière des étoiles : pour un Haut-Elfe, l’obscurité de la nuit sous la lumière des étoiles n’est que de la pénombre, où seuls les petits détails lui échappent.

```

> Noms typiques

```
Les Hauts-elfes privilégient les noms aux sonorités douces mais complexes. L, W, N et M sont les consonnes dominantes et les voyelles sont souvent doubles (ië, aë). Les noms féminins finissent par -wen, -wë, -ië, -aël ou –aëlle. Ceux des hommes par des terminaisons aux sonorités plus tranchées comme -dir, -dur, -dor ou -dil.

Masculin. Arwendil, Caëldwendir, Eldwyndor, Elberenhdir, Elendur, Gilgalendil, Irwildur, Laurelith, Linaëndir, Laendoril, Nennendir, etc.

Féminin. Elberenh, Elidhwen, Ellenúviel, Laurelinn, Linaëwen, Laendorië, Maelwë, Maerwen, Nennenvaël, Ninwelotë, Tintaëlle, etc.
```

### Elfe-Sylvain

```
L’Elfe sylvain est issu d’une culture différente de celle des Hauts-elfes. Légèrement plus petits, ils vivent au plus profond des forêts, s’abritant dans les arbres et vivant simplement de la chasse et de la cueillette. Moins arrogants que leurs cousins Hauts-elfes, ils sont cependant beaucoup plus méfiants. Ils maitrisent particulièrement l’art du camouflage et l’utilisation de l’arc.

Préjugés typiques : les Elfes sylvains sont de redoutables archers, ils détestent les cités et les gens qui y vivent, ils peuvent tuer d’une flèche dans le dos un paysan juste parce qu’il a coupé le mauvais arbre.

```

> Repères

```
Âge de départ : 50+
Espérance de vie : 350 ans
Taille : 1.40 m à 1.70 m
Poids : 30 kg à 60 kg
Svelte et musclé. Oreilles pointues, yeux en amandes (verts, violets), cheveux sombres, tatouages, pilosité absente.

```

> Création de personnage

```
Carac : -2 FOR, +2 DEX
Capacité raciale · Lumière des étoiles : pour un Elfe sylvain, l’obscurité de la nuit sous la lumière des étoiles n’est que de la pénombre, où seuls les petits détails lui échappent.

```

> Noms typiques

```
Les Elfes sylvains ont des noms assez similaires aux Hauts-elfes, mais plus simples. Chez les femmes, la dernière consonne ou la dernière voyelle est doublée. Chez les hommes les terminaisons -din, -dor, -ion sont plus privilégiées.

Masculin. Aëdin, Caëndor, Cirion, Doralion, Ealdor, Findalion, Glorfindor, Haëldion, Laurendor, Morwendir, Raëldirion, etc.

Féminin. Aëldill, Aluinill, Aliann, Dianaë, Eilinelle, Elenwëe, Lúthill, Nínielle, Nolwaënn, Mírielle, Keltienn, etc.
```

### Gnome

```
Le Gnome est une créature de petite taille pourvue d’un gros nez, d’une bonne nature et d’une curiosité insatiable pour la magie et les sciences. C’est un compagnon souvent agréable bien qu’un peu original. Les Nains et les Halfelins l’apprécient, tandis que les « grandes » races se montrent plutôt indifférentes à leur égard.

Préjugés typiques : les Gnomes sont sympathiques mais à moitié fous, méfiez-vous comme de la peste de leurs inventions ou de leurs projets farfelus.

```

> Repères

```
Âge de départ : 40+
Espérance de vie : 250 ans
Taille : 1 m à 1.20 m
Poids : 30 kg à 50 kg
Petit et rondouillard. Gros nez, moustaches et rouflaquettes.

```
>
### Création de personnage

```
Carac : -2 FOR, +2 INT
Capacité raciale · Don étrange : le gnome possède un talent inné pour les sciences, qu’elles soient occultes ou plus ordinaires. Le joueur choisit une des deux options suivantes :
   * soit son personnage gagne un bonus de +5 sur tous les tests d’INT ;
   * soit il choisit une capacité de rang 1 d’Ensorceleur. Il peut utiliser ce sort sans pénalité avec une armure allant jusqu’à la chemise de mailles.

```

> Noms typiques

```
Les Gnomes aiment les prénoms que les humains jugent désuets. Ils ont aussi un nom de famille compliqué et long, qu’ils accolent toujours à ce prénom. Un habitant de notre monde pourrait trouver que ces noms ont des consonances germaniques et étranges.

Masculin. Albert Blumdenplick, Eustache Dimtigballen, Firmin vondemacht, Isidore Kolerbiltag, Nestor Wurmenship, etc.

Féminin. Thérèse Aliserwilp, Huguette Uhldimmerstelp, Rose Molkenpulp, Eugénie Bimpbelinedor, etc.
```

### Halfelin

```
L'Halfelin est la plus petite des races jouables. Toujours bon vivant, souvent vif, curieux, et parfois farceur, c’est un incompris que les autres races considèrent souvent comme turbulent, pénible, voire parfois comme un voleur.

Préjugés typiques : les Halfelins sont inoffensifs et ne pensent qu’à bien manger. Toutefois, ne perdez jamais de vue leurs mains car se sont aussi des voleurs sans scrupules.

```

> Repères

```
Âge de départ : 20+
Espérance de vie : 150 ans
Taille : 0.80 m à 1 m
Poids : 20 kg à 30 kg
Petit et vif. Pieds poilus, regard espiègle.

```

> Création de personnage

```
Carac : -2 FOR, +2 DEX
Capacité raciale · Petite taille : l'Halfelin obtient un bonus de +1 en DEF et de +2 à tous les tests de discrétion. En revanche, un Halfelin peut seulement utiliser à une main une arme dont les DM sont au maximum égaux à 1d6 (épée courte, masse, etc.). Il lui faut utiliser les 2 mains pour les armes qui infligent 1d8 à 1d10 de DM (épée longue). Enfin il lui est interdit d’utiliser les armes qui infligent plus de 1d10 DM.

```

> Noms typiques

```
Les Halfelins mâles utilisent des prénoms courts qui ressemblent à des diminutifs. Les prénoms féminins sont le plus souvent inspirés des fleurs et des fruits. Ils ont généralement un nom en rapport avec leur lieu de naissance : Sur-le-pont, Dubois, Sous-colline, Moulinbas, etc.

Masculin. Bill, Bern, Don, Duky, Eckel, Fili, Ged, Gerry, Hek, Hicks, Jack, Karl, Litle, Luky, Mike, Polo, Sam, Titi, etc.

Féminin. Églantine, Lila, Jacinthe, Marguerite, Muguette, Pâquerette, Prune, Rose, etc
```

### Humain :

```
L’humain se distingue par sa capacité d’adaptation et son instinct qui le pousse à coloniser tous les territoires qui l’entourent. La Race humaine est la plus représentée et la plus répandue dans les zones dites « civilisées ».

Préjugés typiques : les humains sont de jeunes loups ignorants, ils croissent et se multiplient sans aucun respect pour l’équilibre du monde, leur égoïsme et leur égocentrisme semblent sans limite. Ils mèneront le monde entier à la catastrophe si personne ne les arrête.

```

> Repères

```
Âge de départ : 18+
Espérance de vie : 100 ans
Taille : 1.50 m à 2 m
Poids : 40 kg à 120 kg
Toute la diversité possible à l’exception des couleurs trop exotiques (cheveux violets, etc.).


```

> Création de personnage

```
Carac : aucun changement
Capacité raciale · Instinct de survie : lorsqu’une attaque devrait amener le personnage à 0 PV, les DM qu’elle inflige sont divisés par 2 (minimum 1).

```

> Noms typiques

```
Les humains ont les prénoms et les noms les plus variés qui soient. Vous pouvez vous inspirer d’anciens prénoms médiévaux, scandinaves, francs ou celtiques et les modifier légèrement pour leur donner une consonance plus fantastique. Pour les femmes, les noms de minéraux, de fleurs ou de couleurs peuvent aussi êtres utilisés.

Masculin. Adalrik, Arn, Bernulf, Brand, Edwald, Ketil, Ferwin, Gerulf, Godfred, Gunter, Halfdan, Ingvar, Knud, Lothar, Osvald, Roderick, Rurik, Sigfred, Sigmar, Sigvald, Stig, Svenn, Thorsten, etc.

Féminin. Annia, Alaina, Berthille, Bérénia, Télinne, Koralie, Floraline, Gadrielle, Jocynthe, Kéline, Myrthinne, Marille, Naelwen, Natasha, Odaelle, Prescille, Sorsha, Véronelle, Wendoline, etc.
```
### NAIN

```
Le Nain est petit mais robuste. Célèbre pour sa barbe, il aime les profondeurs de la terre, dont il extrait des métaux et des pierres précieuses. Isolé, il est généralement ouvert et chaleureux, mais la société naine peut paraître sévère car le travail et l’entraînement militaire y sont des obligations.

Préjugés typiques : les Nains aiment amasser de l’or et boire de la bière en compagnie d’autres Nains. Ils sont un peu rustres et très susceptibles, un Nain peut se mettre en colère et devenir violent pour des raisons qui semblent étranges voire futiles à un autre peuple…

```

> Repères

```
Âge de départ : 40+
Espérance de vie : 250 ans
Taille : 1.15 m à 1.35 m
Poids : 50 kg à 100 kg
Robuste et trapu. Pilosité très développée, tresses dans les cheveux et la barbe, bijoux et piercing.

```

> Création de personnage

```
Carac : -2 DEX, +2 CON
Capacité raciale · Vision dans le noir : dans le noir total, le Nain voit comme dans la pénombre jusqu’à 30 m.

```

> Noms typiques

```
Les Nains ont des prénoms courts et percutants, les principales terminaisons sont en –IN, -UN, -IK, ou –IR, mais il en existe d’autres. Suit généralement le nom de leur clan, qu’ils traduisent en langue commune pour mettre en avant l’idée qu’il véhicule. Chez les femmes, la terminaison en –HILD ou -ID est la plus courante.

Masculin. Gloin Mâchefer, Krorin Briseroc, Thorin Forgefer, Buldur Tranchetroll, Trorin Cassegranit, Orik Briselame, Durok Ecu-de-chêne, Guerann Marteleur, Korik Peau-de-pierre, etc.

Féminin. Astrid, Arnhild, Berthild, Brynild, Eldrid,  Ermenhild, Frida, Gerda, Grimhild, Gudrun, Helga, Hilda, Ingrid, Klothild, Sigrid, Strida etc.
```

## Classes

[Cliquez Ici](http://chroniques-mondesoublies.com/wiki/index.php?title=PROFILS)


